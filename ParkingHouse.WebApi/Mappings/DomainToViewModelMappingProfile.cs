﻿using AutoMapper;
using ParkingHouse.DTO;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingHouse.WebApi.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Invoice, InvoiceDTO>();
            Mapper.CreateMap<Person, PersonDTO>();
            Mapper.CreateMap<ParkingEntry, ParkingEntryDTO>();
        }
    }
}