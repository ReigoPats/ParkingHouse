﻿using AutoMapper;
using ParkingHouse.DTO;
using ParkingHouse.Model;
using ParkingHouse.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingHouse.WebApi.Controllers
{
    public class PersonController : ApiController
    {
        private readonly IPersonService _personService;


        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        public PersonDTO Get(int id)
        {
            var person = _personService.GetPersonById(id);
            return Mapper.Map<Person, PersonDTO>(person);
        }
    }
}