﻿using AutoMapper;
using ParkingHouse.DTO;
using ParkingHouse.Model;
using ParkingHouse.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingHouse.WebApi.Controllers
{
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceService _invoiceService;
        private readonly IPersonService _personService;
        private readonly IParkingEntryService _parkingEntryService;

        public InvoiceController(IInvoiceService invoiceService, IPersonService personService, IParkingEntryService parkingEntryService)
        {
            _invoiceService = invoiceService;
            _personService = personService;
            _parkingEntryService = parkingEntryService;
        }

        [HttpGet]
        public List<InvoiceDTO> Get(int personId)
        {
            var invoices = _invoiceService.GetInvoicesByPersonId(personId).ToList<Invoice>();
            return Mapper.Map<List<Invoice>, List<InvoiceDTO>>(invoices);
        }

        [HttpPost]
        public void GenereteInvoices()
        {
            var parkingEntries = _parkingEntryService.GetParkingEntriesForInvoiceGeneration();
            List<int> years = parkingEntries.Select(p => p.ParkingStart.Year).Distinct().ToList();
            List<int> months = parkingEntries.Select(p => p.ParkingStart.Month).Distinct().ToList();
            List<int> personIDs = parkingEntries.Select(p => p.PersonId).Distinct().ToList();
            foreach (var personId in personIDs)
            {
                var person = _personService.GetPersonById(personId);
                foreach (var year in years)
                {
                    foreach (var month in months)
                    {
                        var monthsParkingEntriesForPerson = parkingEntries.Where(p => p.PersonId == personId && p.ParkingStart.Year == year && p.ParkingStart.Month == month).ToList();
                        Invoice invoice = new Invoice();
                        invoice.Total = _invoiceService.CalculateTotal(monthsParkingEntriesForPerson, person);
                        invoice.Person = person;
                        invoice.InvoiceNumber = string.Format("{0}-{1}", year, month);
                        _invoiceService.CreateInvoice(invoice);

                        foreach (var parkingEntry in monthsParkingEntriesForPerson)
                        {
                            parkingEntry.Invoice = invoice;
                        }

                        _invoiceService.SaveInvoice();

                    }
                }
            }


        }
    }
}