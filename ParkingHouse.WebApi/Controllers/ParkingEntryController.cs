﻿using AutoMapper;
using ParkingHouse.DTO;
using ParkingHouse.Model;
using ParkingHouse.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingHouse.WebApi.Controllers
{
    public class ParkingEntryController : ApiController
    {
        private readonly IParkingEntryService _parkingEntryService;

        public ParkingEntryController(IParkingEntryService parkingEntryService)
        {
            _parkingEntryService = parkingEntryService;
        }


        [HttpGet]
        public List<ParkingEntryDTO> Get(int personId)
        {
            var parkingEntries = _parkingEntryService.GetParkingEntriesByPersonId(personId).ToList<ParkingEntry>();
            return Mapper.Map<List<ParkingEntry>, List<ParkingEntryDTO>>(parkingEntries);
        }

        [HttpGet]
        public List<ParkingEntryDTO> Get(int invoiceId, int personId)
        {
            var parkingEntries = _parkingEntryService.GetParkingEntriesByInvoiceId(invoiceId).ToList<ParkingEntry>();
            return Mapper.Map<List<ParkingEntry>, List<ParkingEntryDTO>>(parkingEntries);
        }

        public void Post([FromBody]List<ParkingEntryDTO> parkingEntries)
        {
            var parkingEntriesModel = Mapper.Map<List<ParkingEntryDTO>, List<ParkingEntry>>(parkingEntries);
            foreach (var parkingEntryModel in parkingEntriesModel)
            {
                _parkingEntryService.CreateParkingEntry(parkingEntryModel);
                _parkingEntryService.SaveParkingEntry();
            }
        }
    }
}
