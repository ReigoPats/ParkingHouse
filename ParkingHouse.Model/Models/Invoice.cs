﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Model
{
    public class Invoice : BaseModel
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal Total { get; set; }

        public Person Person { get; set; }
        public int PersonId { get; set; }
    }
}
