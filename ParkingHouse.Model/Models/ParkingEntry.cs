﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Model
{
    public class ParkingEntry : BaseModel
    {
        public int Id { get; set; }
        public DateTime ParkingStart { get; set; }
        public DateTime ParkingEnd { get; set; }
        public decimal? Total { get; set; }

        public ParkingLot ParkingLot { get; set; }
        public int ParkingLotId { get; set; }

        public Person Person { get; set; }
        public int PersonId { get; set; }

        public Invoice Invoice { get; set; }
        public int? InvoiceId { get; set; }
    }
}
