﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Model
{
    public class BaseModel
    {
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public BaseModel()
        {
            Created = DateTime.Now;
        }

    }
}
