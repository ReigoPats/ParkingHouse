﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Model
{
    public class ParkingLot : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
