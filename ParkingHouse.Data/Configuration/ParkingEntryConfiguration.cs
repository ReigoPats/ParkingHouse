﻿using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Configuration
{
    public class ParkingEntryConfiguration: EntityTypeConfiguration<ParkingEntry>
    {
        public ParkingEntryConfiguration()
        {
            ToTable("ParkingEntries");
            Property(g => g.ParkingStart).IsRequired();
            Property(g => g.ParkingEnd).IsRequired();
            Property(g => g.ParkingLotId).IsRequired();
            Property(g => g.PersonId).IsRequired();
        }
    }
}
