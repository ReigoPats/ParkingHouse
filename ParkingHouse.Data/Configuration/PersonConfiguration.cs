﻿using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Configuration
{
    public class PersonConfiguration: EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            ToTable("Persons");
            Property(g => g.FirstName).IsRequired().HasMaxLength(500);
            Property(g => g.LastName).IsRequired().HasMaxLength(500);
            Property(g => g.CustomerType).IsRequired();
        }
    }
}
