﻿using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Configuration
{
    public class InvoiceConfiguration: EntityTypeConfiguration<Invoice>
    {
        public InvoiceConfiguration()
        {
            ToTable("Invoices");
            Property(g => g.InvoiceNumber).IsRequired().HasMaxLength(50);
            Property(g => g.PersonId).IsRequired();
            Property(g => g.Total).HasPrecision(8, 2);
                
        }
    }
}
