﻿using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Configuration
{
    public class ParkingLotConfiguration: EntityTypeConfiguration<ParkingLot>
    {
        public ParkingLotConfiguration()
        {
            ToTable("ParkingLots");
            Property(g => g.Name).IsRequired().HasMaxLength(50);
        }
    }
}
