﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        ParkingHouseEntities Init();
    }
}
