﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        ParkingHouseEntities dbContext;

        public ParkingHouseEntities Init()
        {
            if (dbContext == null)
            {
                dbContext = new ParkingHouseEntities();
               // dbContext.Seed();
            }
            return dbContext;
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
