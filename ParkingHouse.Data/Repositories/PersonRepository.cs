﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Repositories
{
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        public PersonRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }

    public interface IPersonRepository : IRepository<Person>
    {

    }
}
