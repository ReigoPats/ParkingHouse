﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Repositories
{
    public class ParkingLotRepository : RepositoryBase<ParkingLot>, IParkingLotRepository
    {
        public ParkingLotRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public ParkingLot GetParkingLotById(int parkingLotId)
        {
            return this.DbContext.ParkingLots.Where(i => i.Id == parkingLotId).SingleOrDefault();
        }
    }

    public interface IParkingLotRepository : IRepository<ParkingLot>
    {
        ParkingLot GetParkingLotById(int parkingLotId);
    }
}
