﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Repositories
{
    public class InvoiceRepository : RepositoryBase<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<Invoice> GetInvoicesByPersonId(int personId)
        {
            return this.DbContext.Invoices.Where(i => i.PersonId == personId);
        }
    }

    public interface IInvoiceRepository : IRepository<Invoice>
    {
        IEnumerable<Invoice> GetInvoicesByPersonId(int personId);
    }
}
