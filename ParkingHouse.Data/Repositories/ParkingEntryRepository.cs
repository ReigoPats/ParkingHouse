﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data.Repositories
{
    public interface IParkingEntryRepository : IRepository<ParkingEntry>
    {
        IEnumerable<ParkingEntry> GetParkingEntriesByInvoiceId(int invoiceId);
        IEnumerable<ParkingEntry> GetParkingEntriesForInvoiceGeneration();
        IEnumerable<ParkingEntry> GetParkingEntriesByPersonId(int personId);
    }

    public class ParkingEntryRepository : RepositoryBase<ParkingEntry>, IParkingEntryRepository
    {
        public ParkingEntryRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<ParkingEntry> GetParkingEntriesByInvoiceId(int invoiceId)
        {
            return this.DbContext.ParkingEntries.Where(i => i.InvoiceId == invoiceId);
        }

        public IEnumerable<ParkingEntry> GetParkingEntriesByPersonId(int personId)
        {
            return this.DbContext.ParkingEntries.Where(i => i.PersonId == personId);
        }

        public IEnumerable<ParkingEntry> GetParkingEntriesForInvoiceGeneration()
        {
            return this.DbContext.ParkingEntries.Where(i => i.InvoiceId == null);
        }
    }
}
