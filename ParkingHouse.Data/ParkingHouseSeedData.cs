﻿using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data
{
    public class ParkingHouseSeedData : DropCreateDatabaseIfModelChanges<ParkingHouseEntities>
    {
        protected override void Seed(ParkingHouseEntities context)
        {
            GetParkingLots().ForEach(pl => context.ParkingLots.Add(pl));
            GetPersons().ForEach(p => context.Persons.Add(p));

            context.Commit();
        }

        private static List<Invoice> GetInvoices() {
            return new List<Invoice> { 
             new Invoice(){
                 Id= 1,
               InvoiceNumber="2017-11-1",
               PersonId= 1
             },
             new Invoice(){
                 Id= 2,
               InvoiceNumber="2017-12-1",
               PersonId= 1
             },
             new Invoice(){
                 Id= 3,
               InvoiceNumber="2017-10-2",
               PersonId= 2
             },
             new Invoice(){
                 Id= 4,
               InvoiceNumber="2017-11-2",
               PersonId= 2
             },
             new Invoice(){
                 Id= 5,
               InvoiceNumber="2017-12-2",
               PersonId= 2
             },
             new Invoice(){
                 Id= 6,
               InvoiceNumber="2018-01-2",
               PersonId= 2
             },
             new Invoice(){
                 Id= 7,
               InvoiceNumber="2017-12-3",
               PersonId= 3
             },
             new Invoice(){
                 Id= 8,
               InvoiceNumber="2017-12-3",
               PersonId= 3
             },
            };
        }

        private static List<Person> GetPersons()
        {
            return new List<Person> { 
              new Person(){
                   FirstName="Malle",
                   LastName= "Maasikas",
                   CustomerType =1,
                   Id=1
              },
              new Person(){
                   FirstName="Kalle",
                   LastName= "Kaalikas",
                   CustomerType =2,
                   Id=2
              },
              new Person(){
                   FirstName="Rikas",
                   LastName= "Robert",
                   CustomerType =2,
                   Id=3
              },
            };
        }

        private static List<ParkingLot> GetParkingLots()
        {
            return new List<ParkingLot> { 
              new ParkingLot(){
                   Name="Kesklinna parkimisemaja",
                   Id=1
              },
            };
        }
    }
}
