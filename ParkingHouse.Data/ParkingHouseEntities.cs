﻿using ParkingHouse.Data.Configuration;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Data
{
    public class ParkingHouseEntities : DbContext
    {
        public ParkingHouseEntities() : base("ParkingHouseEntities") { }

        public DbSet<ParkingLot> ParkingLots { get; set; }
        public DbSet<ParkingEntry> ParkingEntries { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Person> Persons { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new InvoiceConfiguration());
            modelBuilder.Configurations.Add(new ParkingEntryConfiguration());
            modelBuilder.Configurations.Add(new ParkingLotConfiguration());

            modelBuilder.Entity<ParkingEntry>()
            .HasRequired(c => c.Person)
            .WithMany()
            .WillCascadeOnDelete(false);
        }

       /* internal void Seed()
        {
            new ParkingHouseSeedData().Seed(this);
        }*/
    }
}
