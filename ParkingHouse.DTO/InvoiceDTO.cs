﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingHouse.DTO
{
    public class InvoiceDTO
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal Total { get; set; }
        public int PersonId { get; set; }
    }
}