﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.DTO
{
    public class ParkingEntryDTO
    {
        public int Id { get; set; }
        public DateTime ParkingStart { get; set; }
        public DateTime ParkingEnd { get; set; }
        public int ParkingLotId { get; set; }
        public int PersonId { get; set; }
        public decimal? Total { get; set; }
    }
}
