﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using ParkingHouse.Model;

namespace ParkingHouse.WebApi.Tests
{
    [TestClass]
    public class ParkingEntryTest
    {

        ParkingEntryTest() { 
        }

        [TestMethod]
        public void Premium_WhenTotalCostIsOverMaxLimit_ThenMaxLImitIsSetToTotal()
        {
            // arrange  
            var parkingEntries = new List<ParkingEntry> {
            new ParkingEntry(){
                   PersonId=3,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.01.2017 19:40").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.14.2017 20:35").ToLocalTime()
              }};

            // act  

            // assert is handled by ExpectedException  
        }
    }
}
