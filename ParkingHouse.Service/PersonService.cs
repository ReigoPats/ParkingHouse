﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Data.Repositories;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Service
{
    public interface IPersonService
    {
        Person GetPersonById(int id);
        IEnumerable<Person> GetAllPersons();
        void CreatePerson(Person person);
        void SavePerson();
    }

    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PersonService(IPersonRepository personsRepository, IUnitOfWork unitOfWork)
        {
            _personsRepository = personsRepository;
            _unitOfWork = unitOfWork;
        }

        public Person GetPersonById(int id)
        {
            return _personsRepository.GetById(id);
        }

        public IEnumerable<Person> GetAllPersons()
        {
            return _personsRepository.GetAll();
        }

        public void CreatePerson(Person person)
        {
            _personsRepository.Add(person);
        }

        public void SavePerson()
        {
            _unitOfWork.Commit();
        }

    }
}
