﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Data.Repositories;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Service
{
    public interface IParkingEntryService
    {
        ParkingEntry GetParkingEntryById(int id);
        IEnumerable<ParkingEntry> GetParkingEntriesByInvoiceId(int invoiceId);
        IEnumerable<ParkingEntry> GetParkingEntriesByPersonId(int personId);
        void CreateParkingEntry(ParkingEntry parkingEntry);
        void SaveParkingEntry();
        IEnumerable<ParkingEntry> GetParkingEntriesForInvoiceGeneration();
        decimal CalculateParkingEntryTotal(ParkingEntry parkingEntry, Person person);
    }

    public class ParkingEntryService : IParkingEntryService
    {
        private readonly IParkingEntryRepository _parkingEntryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ParkingEntryService(IParkingEntryRepository parkingEntryRepository, IUnitOfWork unitOfWork)
        {
            _parkingEntryRepository = parkingEntryRepository;
            _unitOfWork = unitOfWork;
        }

        public ParkingEntry GetParkingEntryById(int id)
        {
            return _parkingEntryRepository.GetById(id);
        }

        public IEnumerable<ParkingEntry> GetParkingEntriesByInvoiceId(int invoiceId)
        {
            return _parkingEntryRepository.GetParkingEntriesByInvoiceId(invoiceId);
        }

        public IEnumerable<ParkingEntry> GetParkingEntriesByPersonId(int personId)
        {
            return _parkingEntryRepository.GetParkingEntriesByPersonId(personId);
        }

        public void CreateParkingEntry(ParkingEntry parkingEntry)
        {
            _parkingEntryRepository.Add(parkingEntry);
        }

        public void SaveParkingEntry()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<ParkingEntry> GetParkingEntriesForInvoiceGeneration()
        {
            return _parkingEntryRepository.GetParkingEntriesForInvoiceGeneration();
        }


        /// <summary>
        /// Parking fee will be added after ParkingFeeTimeUnitSize minutes has passed
        /// </summary>
        private int ParkingFeeTimeUnitSize = 30;
        private decimal ParkingFeeForEveryTimeUnitForRegular = 1.5M;
        private decimal ParkingFeeForEveryTimeUnitForPremium = 1.0M;
        private decimal ParkingFeeForEveryTimeUnitForRegularNight = 1.0M;
        private decimal ParkingFeeForEveryTimeUnitForPremiumNight = 0.75M;

        private int NightTimeStartHour = 19;
        private int NightTimeEndHour = 7;
        public decimal CalculateParkingEntryTotal(ParkingEntry parkingEntry, Person person)
        {
            DateTime start = parkingEntry.ParkingStart;
            decimal parkingEntryTotal = 0;
            while (start < parkingEntry.ParkingEnd)
            {
                var nightTimeStart = new DateTime(parkingEntry.ParkingStart.Year, parkingEntry.ParkingStart.Month, parkingEntry.ParkingStart.Day).AddHours(NightTimeStartHour);
                var nightTimeEnd = new DateTime(parkingEntry.ParkingStart.Year, parkingEntry.ParkingStart.Month, parkingEntry.ParkingStart.Day).AddHours(NightTimeEndHour);
                if (start < nightTimeStart && parkingEntry.ParkingStart > nightTimeEnd)
                {
                    if (person.CustomerType == 2)
                    {
                        parkingEntryTotal += ParkingFeeForEveryTimeUnitForPremium;
                    }
                    else
                    {
                        parkingEntryTotal += ParkingFeeForEveryTimeUnitForRegular;
                    }
                }
                else
                {
                    if (person.CustomerType == 2)
                    {
                        parkingEntryTotal += ParkingFeeForEveryTimeUnitForPremiumNight;
                    }
                    else
                    {
                        parkingEntryTotal += ParkingFeeForEveryTimeUnitForRegularNight;
                    }
                }

                start = start.AddMinutes(ParkingFeeTimeUnitSize);
            }

            parkingEntry.Total = parkingEntryTotal;
            return parkingEntryTotal;
        }

    }
}
