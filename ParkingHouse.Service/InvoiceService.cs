﻿using ParkingHouse.Data.Infrastructure;
using ParkingHouse.Data.Repositories;
using ParkingHouse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingHouse.Service
{
    public interface IInvoiceService
    {
        Invoice GetInvoiceById(int id);
        IEnumerable<Invoice> GetInvoicesByPersonId(int personId);
        void CreateInvoice(Invoice invoice);
        void SaveInvoice();
        decimal CalculateTotal(List<ParkingEntry> parkingEntries, Person person);
    }

    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IParkingEntryService _parkingEntryService;
        private readonly IUnitOfWork _unitOfWork;

        public InvoiceService(IInvoiceRepository invoiceRepository, IUnitOfWork unitOfWork, IParkingEntryService parkingEntryService)
        {
            _invoiceRepository = invoiceRepository;
            _unitOfWork = unitOfWork;
            _parkingEntryService = parkingEntryService;
        }

        public Invoice GetInvoiceById(int id)
        {
            return _invoiceRepository.GetById(id);
        }

        public IEnumerable<Invoice> GetInvoicesByPersonId(int personId)
        {
            return _invoiceRepository.GetInvoicesByPersonId(personId);
        }

        public void CreateInvoice(Invoice invoice)
        {
            _invoiceRepository.Add(invoice);
        }

        public void SaveInvoice()
        {
            _unitOfWork.Commit();
        }

        public decimal CalculateTotal(List<ParkingEntry> parkingEntries, Person person)
        {
            decimal total = 0;
            //Parking entries Cost
            foreach (var parkingEntry in parkingEntries)
            {
                total += _parkingEntryService.CalculateParkingEntryTotal(parkingEntry, person);
            }

            //monthly fee
            if (person.CustomerType == 2)
            {
                total += 20;
            }

            //max Total size
            if (person.CustomerType == 2 && total > 300)
            {
                total = 300;
            }


            return total;
        }

    }
}
