﻿using AutoMapper;
using ParkingHouse.DTO;
using ParkingHouse.Service;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParkingHouse.Web.Controllers
{
    public class HomeController : Controller
    {
        private string _webApiLocation = "http://localhost:51648/api";
        RestClient _webApi;

        public int PersonId
        {
            get { return Session["PersonId"] != null ? (int)Session["PersonId"] : 1; }
            set { Session["PersonId"] = value; }
        }

        public HomeController()
        {
            _webApi = new RestClient(_webApiLocation);
        }


        // GET: Home
        public ActionResult Index(int? personId)
        {
            if (personId.HasValue)
            {
                PersonId = (int)personId;
            }

            PersonDTO viewModelPerson = new PersonDTO();

            //getting user data
            var request = new RestRequest("Person", Method.GET);
            request.AddParameter("Id", PersonId);
            var responseUser = _webApi.Execute<PersonDTO>(request);
            if (responseUser.Data != null)
            {
                viewModelPerson = responseUser.Data;
            }

            //getting user invoices
            request = new RestRequest("Invoice", Method.GET);
            request.AddParameter("personId", PersonId);

            var responseInvoices = _webApi.Execute<List<InvoiceDTO>>(request);
            if (responseInvoices.Data != null)
            {
                viewModelPerson.Invoices = responseInvoices.Data;
            }

            return View(viewModelPerson);
        }

        public ActionResult ParkingInfo()
        {
            //getting parkingInfo
            var request = new RestRequest("ParkingEntry", Method.GET);
            request.AddParameter("personId", PersonId);
            var response = _webApi.Execute<List<ParkingEntryDTO>>(request);
            if (response.Data != null)
            {
                return View(response.Data.OrderBy(p => p.ParkingStart).ToList());
            }
            else {
                return View(new List<ParkingEntryDTO>());
            }
        }

        public ActionResult InvoiceDetail(int id)
        {
            //getting parkingInfo for infvoice
            var request = new RestRequest("ParkingEntry", Method.GET);
            request.AddParameter("invoiceId", id);
            request.AddParameter("personId", PersonId);
            var response = _webApi.Execute<List<ParkingEntryDTO>>(request);
            if (response.Data != null)
            {
                return View(response.Data);
            }
            else
            {
                return View(new List<ParkingEntryDTO>());
            }
        }


        /// <summary>
        /// Simulating Importing 
        /// </summary>
        /// <returns></returns>
        public ActionResult Import()
        {
            //importing parking entries
            var request = new RestRequest("ParkingEntry", Method.POST);
            var parkingEntries = GetParkingEntries();
            request.AddJsonBody(parkingEntries);
            var response = _webApi.Execute(request);

            //generating invoices from parking entries
            request = new RestRequest("Invoice/GenereteInvoices", Method.POST);
            response = _webApi.Execute(request);

            return RedirectToAction("Index");
        }


        /// <summary>
        /// ParkingEntries For Simulating Import
        /// </summary>
        /// <returns></returns>
        private static List<ParkingEntryDTO> GetParkingEntries()
        {
            return new List<ParkingEntryDTO> { 
              new ParkingEntryDTO(){
                   PersonId=1,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.21.2017 08:12").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.21.2017 10:45").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=1,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.22.2017 19:40").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.22.2017 20:35").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=2,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.11.2017 08:12").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.11.2017 10:45").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=2,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.12.2017 07:02").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.12.2017 11:56").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=2,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.13.2017 22:10").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.13.2017 22:35").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=2,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.14.2017 19:40").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.14.2017 20:35").ToLocalTime()
              },
              new ParkingEntryDTO(){
                   PersonId=3,
                   ParkingLotId=1,
                   ParkingStart= DateTime.Parse("10.01.2017 19:40").ToLocalTime(),
                   ParkingEnd = DateTime.Parse("10.14.2017 20:35").ToLocalTime()
              },
            };
        }        
    }
}